const keys = document.querySelectorAll(".key");

const getRandomNumber = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const getRandomKey = () => keys[getRandomNumber(0, keys.length - 1)]

const targetRandomKey = () => {
    const key = document.getElementById(getRandomKey().getAttribute("id"));
    key.classList.add("selected")
}

document.addEventListener("keydown", (ev) => {
    const selectedKey = document.querySelector(".selected");
    const pressedKey = document.getElementById(ev.code);

    if (pressedKey === selectedKey) {
        selectedKey.classList.remove("selected");
        targetRandomKey();
    }

    keys.forEach(item => {
        if (ev.code === item.id) {
            item.classList.add("hit")
            item.addEventListener('animationend', () => {
                item.classList.remove("hit")
            })
        }
    })
})

targetRandomKey();